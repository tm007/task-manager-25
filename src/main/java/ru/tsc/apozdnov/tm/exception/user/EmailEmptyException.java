package ru.tsc.apozdnov.tm.exception.user;

public class EmailEmptyException extends AbstractUserException {

    public EmailEmptyException() {
        super("Error!!! Email is empty !");
    }

}
