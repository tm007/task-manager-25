package ru.tsc.apozdnov.tm.api.model;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

public interface IHasDateBegin {

    @NotNull
    Date getDateBegin();

    void setDateBegin(@NotNull Date dateBegin);

}
