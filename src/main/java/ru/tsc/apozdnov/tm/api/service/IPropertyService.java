package ru.tsc.apozdnov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

}
