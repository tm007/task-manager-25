package ru.tsc.apozdnov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.model.Task;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Show task by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("***** SHOW TASK BY INDEX ****");
        System.out.println("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        @Nullable final Task task = serviceLocator.getTaskService().findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
