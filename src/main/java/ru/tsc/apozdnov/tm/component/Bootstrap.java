package ru.tsc.apozdnov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.apozdnov.tm.api.repository.ICommandRepository;
import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;
import ru.tsc.apozdnov.tm.api.repository.ITaskRepository;
import ru.tsc.apozdnov.tm.api.repository.IUserRepository;
import ru.tsc.apozdnov.tm.api.service.*;
import ru.tsc.apozdnov.tm.command.AbstractCommand;
import ru.tsc.apozdnov.tm.command.project.*;
import ru.tsc.apozdnov.tm.command.system.*;
import ru.tsc.apozdnov.tm.command.task.*;
import ru.tsc.apozdnov.tm.command.user.*;
import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.apozdnov.tm.exception.system.CommandNotSupportedException;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.model.User;
import ru.tsc.apozdnov.tm.repository.CommandRepository;
import ru.tsc.apozdnov.tm.repository.ProjectRepository;
import ru.tsc.apozdnov.tm.repository.TaskRepository;
import ru.tsc.apozdnov.tm.repository.UserRepository;
import ru.tsc.apozdnov.tm.service.*;
import ru.tsc.apozdnov.tm.util.DateUtil;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Objects;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMAND = "ru.tsc.apozdnov.tm.command";

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, taskRepository, projectRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMAND);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class clazz : classes) {
            registry(clazz);
        }
    }

    @SneakyThrows
    private void registry(@NotNull final Class clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final Object objects = clazz.newInstance();
        final AbstractCommand command = (AbstractCommand) objects;
        registry(command);
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initDemoData() {
        @NotNull User test = userService.create("test", "test", "test@test.java");
        @NotNull User root = userService.create("root", "root", RoleType.ADMIN);
        projectService.create(test.getId(), "silver", "*", DateUtil.toDate("18.08.2012"), DateUtil.toDate("18.08.2021"));
        projectService.create(test.getId(), "berkut", "*", DateUtil.toDate("13.09.2012"), DateUtil.toDate("18.08.2023"));
        projectService.create(root.getId(), "global", "*", DateUtil.toDate("12.04.2012"), DateUtil.toDate("18.08.2024"));
        projectService.create(root.getId(), "suprime", "*", DateUtil.toDate("11.01.2013"), DateUtil.toDate("18.08.2025"));
        taskService.create("bTASK01", "T01");
        taskService.create("aTASK02", "T02");
        taskService.create("zTASK03", "T03");
    }

    private void iniLogger() {
        loggerService.info("**** Welcome to Task Manager ****");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("**** ShitDown Task Manager ****");
            }
        });
    }

    public void run(@Nullable final String[] args) {
        if (processArgumentTask(args)) System.exit(0);
        initDemoData();
        iniLogger();
        while (true) {
            try {
                System.out.println("Enter command:");
                @NotNull final String cmd = TerminalUtil.nextLine();
                processCommandTask(cmd);
                System.out.println("****OK****");
                loggerService.command(cmd);
            } catch (Exception ex) {
                loggerService.error(ex);
                System.err.println("***FAULT****");
            }
        }
    }

    public void processCommandTask(@NotNull final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoleType());
        abstractCommand.execute();
    }

    public void processArgumentTask(@NotNull final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public boolean processArgumentTask(final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgumentTask(arg);
        return true;
    }

}
