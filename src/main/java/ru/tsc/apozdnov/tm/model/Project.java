package ru.tsc.apozdnov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.model.IWBS;
import ru.tsc.apozdnov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String descriprion = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private Date dateCreated = new Date();

    @Nullable
    private Date dateBegin = new Date();

    @Nullable
    private Date dateEnd = new Date();

    public Project(@NotNull final String name,
                   @NotNull final String descriprion,
                   @NotNull final Status status,
                   @Nullable final Date dateBegin) {
        this.name = name;
        this.descriprion = descriprion;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    @Override
    public String toString() {
        return "NAME" + ": " + name + " |" +
                "DESCRIPTION: " + ": " + descriprion + " | " +
                "STATUS:" + " " + getStatus().getDisplayName() + "|" +
                "DATE BEGIN:" + " " + dateBegin + "\n";
    }

}
